$( document ).ready(function() {
    getClients();
    getTickets();
    getMeetings();
    getContacts();
    getUsers();
    $('.datepicker').datepicker();
});

//GETS

//Users
function getUsers() {
    debugger;
    $.ajax({
    method: "GET",
    url: "http://localhost:3000/users"
    }).done(function( response ) {
        console.log(response);
        //setTimeout(getUsers, 70000);
        renderUsersTable(response);
    }).fail(function (error){
    console.log(error);
    });
}

function renderUsersTable(listOfUsers) {
    debugger;
    $('#UsersOutPut').empty();
    var row = "<tr>";
        row += "<td contenteditable='true' id='new_name_user'>new name</td>";
        row += "<td contenteditable='true' id='new_lastname_user'>new lastname</td>";
        row += "<td contenteditable='true' id='new_username_user'>new username</td>";
        row += "<td contenteditable='true' id='new_email_user'>new email</td>";
        row += "<td contenteditable='true' id='new_password_user'>new password</td>";
        row += "<td contenteditable='true' id='new_verification_password_user'>verify your password</td>";
        row += "<td><a onclick='saveUser()'><i class='material-icons'>save</i></a></td>";
        row += "</tr>";
        $("#UsersOutPut").append(row);
    listOfUsers.forEach(function(element) {
        row = "<tr>";
        row += "<td contenteditable='true' id='name_user"+element.id+"'>"+element.name+"</td>";
        row += "<td contenteditable='true' id='lastname_user"+element.id+"'>"+element.lastname+"</td>";
        row += "<td contenteditable='true' id='username_user"+element.id+"'>"+element.username+"</td>";
        row += "<td contenteditable='true' id='email_user"+element.id+"'>"+element.email+"</td>";
        row += "<td><a onclick='updateUser("+element.id+")'><i class='material-icons'>save</i></a><a onclick='deleteUser("+element.id+")'><i class='material-icons'>delete_forever</i></a></td>";
        row += "<td></td>";
        row += "<td></td>";
        row += "</tr>";
    $("#UsersOutPut").append(row);
    });
    M.toast({html: 'User\'s list updated'})
}

//get a foreign value
function getObjectById(url, elementId){
    debugger;
    $.ajax({
        method: "GET",
        url: url
        }).done(function(response) {
            debugger;
            console.log(response);
            $("#"+elementId).append(response.name);
        }).fail(function (error){
        console.log(error);
    });
}

//Clients
function getClients() {
    debugger;
    $.ajax({
    method: "GET",
    url: "http://localhost:3000/clients"
    }).done(function(response) {
        console.log(response);
        //setTimeout(getClients, 70000);
        renderClientsTable(response);
    }).fail(function (error){
    console.log(error);
    });
}

function renderClientsTable(listOfClients) {
    debugger;
    $('#ClientsOutPut').empty();
        var row = "<tr><div id='new_sector_client'></div></tr><tr>";
        row += "<td contenteditable='true' id='new_name_client'>new name</td>";
        row += "<td contenteditable='true' id='new_dni_client'>new dni</td>";
        row += "<td contenteditable='true' id='new_website_client'>new website</td>";
        row += "<td contenteditable='true' id='new_address_client'>new address</td>";
        row += "<td contenteditable='true' id='new_phone_number_client'> new phone number</td>";
        row += "<td id='new_sector_client'></td>";
        row += "<td><a onclick='saveClient()'><i class='material-icons'>save</i></a></td>";
        row += "</tr>";
        $("#ClientsOutPut").append(row);
        getSectors();
        //$('#new_sector_client').hide();
    listOfClients.forEach(function(element) {
        debugger;
        var urlId = "http://localhost:3000/sectors/"+element.sector_id;
        row = "<tr>";
        row += "<td contenteditable='true' id='name_client"+element.id+"'>"+element.name+"</td>";
        row += "<td contenteditable='true' id='dni_client"+element.id+"'>"+element.dni+"</td>";
        row += "<td contenteditable='true' id='website_client"+element.id+"'>"+element.website+"</td>";
        row += "<td contenteditable='true' id='address_client"+element.id+"'>"+element.address+"</td>";
        row += "<td contenteditable='true' id='phone_number_client"+element.id+"'>"+element.phone_number+"</td>";
        row += "<td id='sector_name_client"+element.id+"'></td>";
        row += "<td><a onclick='saveClient("+element.id+")'><i class='material-icons'>save</i></a><a onclick='deleteClient("+element.id+")'><i class='material-icons'>delete_forever</i></a></td>";
        getObjectById(urlId, "sector_name_client"+element.id);
        row += "</tr>";
    $("#ClientsOutPut").append(row);
    });
    M.toast({html: 'Client\'s list updated'})
    //$('#new_sector_client').show();
}

//Contacts
function getContacts() {
    debugger;
    $.ajax({
    method: "GET",
    url: "http://localhost:3000/contacts"
    }).done(function( response ) {
        console.log(response);
        //setTimeout(renderContactsTable, 70000);
        renderContactsTable(response);
    }).fail(function (error){
    console.log(error);
    });
}

function renderContactsTable(listOfContatcs) {
    debugger;
    $('#ContactsOutPut').empty();
    listOfContatcs.forEach(function(element) {
        debugger;
        var positionId = "http://localhost:3000/positions/"+element.position_id;
        var clientId = "http://localhost:3000/clients/"+element.client_id;
        var row = "<tr>";
        row += "<td contenteditable='true' id='name_contact"+element.id+"'>"+element.name+"</td>";
        row += "<td contenteditable='true' id='lastname_contact"+element.id+"'>"+element.lastname+"</td>";
        row += "<td contenteditable='true' id='email_contact"+element.id+"'>"+element.email+"</td>";
        row += "<td contenteditable='true' id='phone_number_contact"+element.id+"'>"+element.phone_number+"</td>";
        row += "<td id='position_name_contact"+element.id+"'></td>";
        row += "<td id='client_name_contact"+element.id+"'></td>";
        row += "<td><a onclick='saveContact("+element.id+")'><i class='material-icons'>save</i></a><a onclick='deleteContact("+element.id+")'><i class='material-icons'>delete_forever</i></a></td>";
        getObjectById(positionId, "position_name_contact"+element.id);
        getObjectById(clientId, "client_name_contact"+element.id);
        row += "</tr>";
    $("#ContactsOutPut").append(row);
    });
    M.toast({html: 'Contact\'s list updated'})
}

//Meetings
function getMeetings() {
    debugger;
    $.ajax({
    method: "GET",
    url: "http://localhost:3000/meetings"
    }).done(function( response ) {
        console.log(response);
        //setTimeout(getMeetings, 70000);
        renderMeetingsTable(response);
    }).fail(function (error){
    console.log(error);
    });
}

function renderMeetingsTable(listOfMeetings) {
    debugger;
    $('#MeetingsOutPut').empty();
    listOfMeetings.forEach(function(element) {
        debugger;
        var row = "<tr>";
        row += "<td>"+element.title+"</td>";
        row += "<td ><input type='text' class='datetimepicker'></td>";
        if(element.virtual){
            row += "<td>Si</td>";
        }else{
            row += "<td>No</td>";
        }
        row += "<td><a onclick='viewMeeting("+element.id+")'><i class='material-icons'>pageview</i></a><a onclick='deleteMeeting("+element.id+")'><i class='material-icons'>delete_forever</i></a></td>";
        row += "<tr/>";
    $("#MeetingsOutPut").append(row);
    });
    M.toast({html: 'Meetings\'s list updated'})
}

//Tickets
function getTickets() {
    $.ajax({
    method: "GET",
    url: "http://localhost:3000/tickets"
    }).done(function( response ) {
        console.log(response);
        //setTimeout(getTickets, 70000);
        renderTicketsTable(response);
    }).fail(function (error){
    console.log(error);
    });
}

function renderTicketsTable(listOfTickets) {
    debugger;
    $('#TicketsOutPut').empty();
    loadClients();
    loadStates();
    loadUsers();
    var row = "<tr>";
        row += "<td contenteditable='true' id='new_title_ticket'>new title</td>";
        row += "<td contenteditable='true' id='new_detail_ticket'>new detail</td>";
        row += "<td contenteditable='true' id='new_user_ticket'></td>";
        row += "<td contenteditable='true' id='new_state_ticket'></td>";
        row += "<td contenteditable='true' id='new_client_ticket'></td>";
        row += "<td><a onclick='saveTicket()'><i class='material-icons'>save</i></a></td>";
        row += "</tr>";
        $("#TicketsOutPut").append(row);
    listOfTickets.forEach(function(element) {
        debugger;
        var stateId = "http://localhost:3000/states/"+element.state_id;
        var clientId = "http://localhost:3000/clients/"+element.client_id;
        var userId = "http://localhost:3000/users/"+element.user_id;
        row = "<tr>";
        var states = getObjectById(0);
        row += "<td contenteditable='true' id='title_ticket"+element.id+"'>"+element.title+"</td>";
        row += "<td contenteditable='true' id='detail_ticket"+element.id+"'>"+element.detail+"</td>";
        row += "<td id='user_name_ticket"+element.id+"'></td>";
        row += "<td id='state_name_ticket"+element.id+"'></td>";
        row += "<td id='client_name_ticket"+element.id+"'></td>";
        row += "<td><a onclick='saveTicket("+element.id+")'><i class='material-icons'>save</i></a><a onclick='deleteTicket("+element.id+")'><i class='material-icons'>delete_forever</i></a></td>";
        getObjectById(userId, "user_name_ticket"+element.id);
        getObjectById(stateId, "state_name_ticket"+element.id);
        getObjectById(clientId, "client_name_ticket"+element.id);
        row += "</tr>";
    $("#TicketsOutPut").append(row);
    });
    M.toast({html: 'Ticket\'s list updated'})
}

//DELETES

//Users
function deleteUser(id) {
    debugger;
    $.ajax({
    method: "DELETE",
    url: "http://localhost:3000/users/" + id + "",
    }).done(function( response ) {
        console.log(response);
        getUsers();
        M.toast({html: 'User deleted'})
    }).fail(function (error){
        console.log(error);
    });
}

//Clients
function deleteClient(id) {
    debugger;
    $.ajax({
    method: "DELETE",
    url: "http://localhost:3000/clients/" + id + "",
    }).done(function( response ) {
        console.log(response);
        getClients();
        M.toast({html: 'Client deleted'})
    }).fail(function (error){
        console.log(error);
    });
}

//Contacts
function deleteContact(id) {
    debugger;
    $.ajax({
    method: "DELETE",
    url: "http://localhost:3000/contacts/" + id + "",
    }).done(function( response ) {
        console.log(response);
        getContacts();
        M.toast({html: 'Contact deleted'})
    }).fail(function (error){
        console.log(error);
    });
}

//Meetings
function deleteMeeting(id) {
    debugger;
    $.ajax({
    method: "DELETE",
    url: "http://localhost:3000/meetings/" + id + "",
    }).done(function( response ) {
        console.log(response);
        getMeeting();
        M.toast({html: 'Meeting deleted'})
    }).fail(function (error){
        console.log(error);
    });
}

//Tickets
function deleteTicket(id) {
    debugger;
    $.ajax({
    method: "DELETE",
    url: "http://localhost:3000/tickets/" + id + "",
    }).done(function( response ) {
        console.log(response);
        getTickets();
        M.toast({html: 'Ticket deleted'})
    }).fail(function (error){
        console.log(error);
    });
}


//POST

//Users
function saveUser() {
    debugger;
    var password = $("#new_password_user").html();
    var verification_password = $("#new_verification_password_user").html();
    var user = {};
    user.name = $("#new_name_user").html();
    user.lastname = $("#new_lastname_user").html();
    user.email = $("#new_email_user").html();
    user.username = $("#new_username_user").html();
    if(password==verification_password){
        user.password_digest = password;
        $.ajax({
        method: "POST",
        url: "http://localhost:3000/users/",
        data: {
            "user": user
        }
        }).done(function( response ) {
            console.log(response);
            getUsers();
            M.toast({html: 'User created'})
        }).fail(function (error){
        console.log(error);
        });
    }else{
        M.toast({html: 'Please, verify your password!'})
    }
}

//Clients
function saveClient() {
    debugger;
    var client = {};
    client.name = $("#new_name_client").html();
    client.dni = $("#new-dni_client").html();
    client.phone_number = $("#new_phone_number_client").html();
    client.address = $("#new_address_client").html();
    client.website = $("#new_website_client").html();
    client.sector_id = $("#new_sector_id").val();
    $.ajax({
    method: "POST",
    url: "http://localhost:3000/clients/",
    data: {
        "client": client
    }
    }).done(function( response ) {
        console.log(response);
        getClients();
        M.toast({html: 'Client created'})
    }).fail(function (error){
        console.log(error);
        M.toast({html: 'Please, verify the information'})
    });
}

function saveTicket() {
    debugger;
    var ticket = {};
    ticket.title = $("#new_title_ticket").html();
    ticket.detail = $("#new_detail_ticket").html();
    ticket.user_id = $("#new_user_id").val();
    ticket.state_id = $("#new_state_id").val();
    ticket.client_id = $("#new_client_id").val();
    $.ajax({
    method: "POST",
    url: "http://localhost:3000/tickets",
    data: {
        "ticket": ticket
    }
    }).done(function( response ) {
        console.log(response);
        getTickets();
        M.toast({html: 'Ticket created'})
    }).fail(function (error){
        console.log(error);
        M.toast({html: 'Please, verify the information'})
    });
}

//Contacts
function saveContact(id) {
    debugger;
    var contact = {};
    contact.name = $("#name_contact"+id).html();
    contact.lastname = $("#lastnamename_contact"+id).html();
    contact.phone_number = $("#phone_number_contact"+id).html();
    contact.email = $("#email_contact"+id).html();
    $.ajax({
    method: "PUT",
    url: "http://localhost:3000/contacts/"+id,
    data: {
        "contact": contact
    }
    }).done(function( response ) {
        console.log(response);
        getContacts();
        M.toast({html: 'Contact modified'})
    }).fail(function (error){
        console.log(error);
        M.toast({html: 'Please, verify the information'})
    });
}

//Ticket
/*function saveTicket(id) {
    debugger;
    var ticket = {};
    ticket.title = $("#title_ticket"+id).html();
    ticket.detail = $("#detail_ticket"+id).html();
    $.ajax({
    method: "PUT",
    url: "http://localhost:3000/tickets/"+id,
    data: {
        "ticket": ticket
    }
    }).done(function( response ) {
        console.log(response);
        getTickets();
        M.toast({html: 'Ticket modified'})
    }).fail(function (error){
    console.log(error);
    });
}*/


//PUT
function updateUser(id) {
    debugger;
    var user = {};
    user.name = $("#name_user"+id).html();
    user.lastname = $("#lastname_user"+id).html();
    user.email = $("#email_user"+id).html();
    user.username = $("#username_user"+id).html();
        $.ajax({
        method: "PUT",
        url: "http://localhost:3000/users/"+id,
        data: {
            "user": user
        }
        }).done(function( response ) {
            console.log(response);
            getUsers();
            M.toast({html: 'User updated!'})
        }).fail(function (error){
        console.log(error);
        });
}

//UTILS

//loadStates
/*function getUsers() {
    debugger;
    $.ajax({
    method: "GET",
    url: "http://localhost:3000/users"
    }).done(function( response ) {
        console.log(response);
        //setTimeout(getUsers, 70000);
        renderUsersTable(response);
    }).fail(function (error){
    console.log(error);
    });
}*/

//LoadClients


//LoadSectors
function getSectors() {
    debugger;
    $.ajax({
    method: "GET",
    url: "http://localhost:3000/sectors"
    }).done(function( response ) {
        console.log(response);
        //setTimeout(getUsers, 70000);
        renderSectors(response);
    }).fail(function (error){
    console.log(error);
    });
}

function renderSectors(listOfSectors) {
    debugger;
    $('#new_sector_client').empty();
    var row = "<div style='width:100%;' class='input-field col s12'><select id='new_sector_id' style='display: block; width:100%;'>";
    row += "<option style='width:100%;' value='0' selected>Choose the sector</option>";
    listOfSectors.forEach(function(element) {
        row += "<option style='width:100%;' value='"+element.id+"'>"+element.name+"</option>";
    });
    row += "</select></div>";
    $("#new_sector_client").append(row);
}

//LoadPositions


//LoadUsers

function loadUsers() {
    debugger;
    $.ajax({
    method: "GET",
    url: "http://localhost:3000/users"
    }).done(function( response ) {
        console.log(response);
        //setTimeout(getUsers, 70000);
        renderUsers(response);
    }).fail(function (error){
    console.log(error);
    });
}

function renderUsers(listOfUsers) {
    debugger;
    $('#new_user_ticket').empty();
    var row = "<div style='width:100%;' class='input-field col s12'><select id='new_user_id' style='display: block; width:100%;'>";
    row += "<option style='width:100%;' value='0' selected>Choose the user</option>";
    listOfUsers.forEach(function(element) {
        row += "<option style='width:100%;' value='"+element.id+"'>"+element.name+" "+element.lastname+"</option>";
    });
    row += "</select></div>";
    $("#new_user_ticket").append(row);
}

function loadClients() {
    debugger;
    $.ajax({
    method: "GET",
    url: "http://localhost:3000/clients"
    }).done(function( response ) {
        console.log(response);
        //setTimeout(getUsers, 70000);
        renderClients(response);
    }).fail(function (error){
    console.log(error);
    });
}

function renderClients(listOfClients) {
    debugger;
    $('#new_client_ticket').empty();
    var row = "<div style='width:100%;' class='input-field col s12'><select id='new_client_id' style='display: block; width:100%;'>";
    row += "<option style='width:100%;' value='0' selected>Choose the client</option>";
    listOfClients.forEach(function(element) {
        row += "<option style='width:100%;' value='"+element.id+"'>"+element.name+"</option>";
    });
    row += "</select></div>";
    $("#new_client_ticket").append(row);
}

function loadStates() {
    debugger;
    $.ajax({
    method: "GET",
    url: "http://localhost:3000/states"
    }).done(function( response ) {
        console.log(response);
        //setTimeout(getUsers, 70000);
        renderStates(response);
    }).fail(function (error){
    console.log(error);
    });
}

function renderStates(listOfStates) {
    debugger;
    $('#new_state_ticket').empty();
    var row = "<div style='width:100%;' class='input-field col s12'><select id='new_state_id' style='display: block; width:100%;'>";
    row += "<option style='width:100%;' value='0' selected>Choose the state</option>";
    listOfStates.forEach(function(element) {
        row += "<option style='width:100%;' value='"+element.id+"'>"+element.name+"</option>";
    });
    row += "</select></div>";
    $("#new_state_ticket").append(row);
}

function loadPositions() {
    debugger;
    $.ajax({
    method: "GET",
    url: "http://localhost:3000/states"
    }).done(function( response ) {
        console.log(response);
        //setTimeout(getUsers, 70000);
        renderPositions(response);
    }).fail(function (error){
    console.log(error);
    });
}

function renderPositions(listOfPositions) {
    debugger;
    $('#new_state_ticket').empty();
    var row = "<div style='width:100%;' class='input-field col s12'><select id='new_state_id' style='display: block; width:100%;'>";
    row += "<option style='width:100%;' value='0' selected>Choose the state</option>";
    listOfPositions.forEach(function(element) {
        row += "<option style='width:100%;' value='"+element.id+"'>"+element.name+"</option>";
    });
    row += "</select></div>";
    $("#new_state_ticket").append(row);
}
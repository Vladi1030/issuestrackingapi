require 'test_helper'

class UserpermeetingsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @userpermeeting = userpermeetings(:one)
  end

  test "should get index" do
    get userpermeetings_url, as: :json
    assert_response :success
  end

  test "should create userpermeeting" do
    assert_difference('Userpermeeting.count') do
      post userpermeetings_url, params: { userpermeeting: { meeting_id: @userpermeeting.meeting_id, user_id: @userpermeeting.user_id } }, as: :json
    end

    assert_response 201
  end

  test "should show userpermeeting" do
    get userpermeeting_url(@userpermeeting), as: :json
    assert_response :success
  end

  test "should update userpermeeting" do
    patch userpermeeting_url(@userpermeeting), params: { userpermeeting: { meeting_id: @userpermeeting.meeting_id, user_id: @userpermeeting.user_id } }, as: :json
    assert_response 200
  end

  test "should destroy userpermeeting" do
    assert_difference('Userpermeeting.count', -1) do
      delete userpermeeting_url(@userpermeeting), as: :json
    end

    assert_response 204
  end
end

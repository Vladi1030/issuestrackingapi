class CreateUserpermeetings < ActiveRecord::Migration[5.1]
  def change
    create_table :userpermeetings do |t|
      t.references :meeting, foreign_key: true
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end

class CreateClients < ActiveRecord::Migration[5.1]
  def change
    create_table :clients do |t|
      t.string :name
      t.string :dni
      t.string :website
      t.string :address
      t.string :phone_number
      t.references :sector, foreign_key: true

      t.timestamps
    end
  end
end

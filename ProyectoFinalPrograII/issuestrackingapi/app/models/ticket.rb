class Ticket < ApplicationRecord
  belongs_to :user
  belongs_to :client
  belongs_to :state
end

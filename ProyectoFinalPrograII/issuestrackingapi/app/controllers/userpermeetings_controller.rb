class UserpermeetingsController < ApplicationController
  before_action :set_userpermeeting, only: [:show, :update, :destroy]

  # GET /userpermeetings
  def index
    @userpermeetings = Userpermeeting.all

    render json: @userpermeetings
  end

  # GET /userpermeetings/1
  def show
    render json: @userpermeeting
  end

  # POST /userpermeetings
  def create
    @userpermeeting = Userpermeeting.new(userpermeeting_params)

    if @userpermeeting.save
      render json: @userpermeeting, status: :created, location: @userpermeeting
    else
      render json: @userpermeeting.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /userpermeetings/1
  def update
    if @userpermeeting.update(userpermeeting_params)
      render json: @userpermeeting
    else
      render json: @userpermeeting.errors, status: :unprocessable_entity
    end
  end

  # DELETE /userpermeetings/1
  def destroy
    @userpermeeting.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_userpermeeting
      @userpermeeting = Userpermeeting.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def userpermeeting_params
      params.require(:userpermeeting).permit(:meeting_id, :user_id)
    end
end

Rails.application.routes.draw do
  resources :sessions
  resources :userpermeetings
  resources :tickets
  resources :meetings
  resources :contacts
  resources :clients
  resources :states
  resources :positions
  resources :sectors
  resources :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
